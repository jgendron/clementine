package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import library.ActiviteDAO;
import library.SeniorDAO;
import model.Activite;
import model.Senior;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.JList;

public class SupprimerActivite extends JFrame {

	private JPanel contentPane;
	private JButton btnSupprimer;
	private static JList<String> listActivite;
	private static JScrollPane scrollPane;
	
	
	public static void setListActivite(List<Activite> liste) {
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Activite s : liste) {
			model.addElement(s.getDesignation());
		}
		listActivite.setModel(model);
	}
	
	/**
	 * Create the frame.
	 */
	public SupprimerActivite(List<Activite> liste) {
		setTitle("Activite - Supprimer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnSupprimer = new JButton("Supprimer");
		btnSupprimer.setBounds(62, 203, 110, 47);
		contentPane.add(btnSupprimer);
		
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnQuitter.setBounds(231, 203, 110, 47);
		contentPane.add(btnQuitter);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(50, 26, 326, 154);
		
		listActivite = new JList();
		listActivite.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(listActivite);
		contentPane.add(scrollPane);
		
		setListActivite(liste);

	}
	
	public static int getActSelect() throws Exception{
		List<Activite> liste=null;
		liste = ActiviteDAO.getAll();
		int i = listActivite.getSelectedIndex();
		Activite s = liste.get(i);
		int id = s.getId();
		return id;
	}
	
	public void assignListener(Ctrl ctrl) {
		this.btnSupprimer.setActionCommand("SupprimerActivite_suppr");
		this.btnSupprimer.addActionListener(ctrl);
	}
}
