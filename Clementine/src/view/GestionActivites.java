package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GestionActivites extends JFrame {

	private JPanel contentPane;
	private JButton btnAjouterActivité;
	private JButton btnSupprimerActivité;


	/**
	 * Create the frame.
	 */
	public GestionActivites() {
		setTitle("gestionActivite");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnAjouterActivité = new JButton("Enregistrer nouvelle activit\u00E9");
		btnAjouterActivité.setBounds(53, 28, 305, 37);
		contentPane.add(btnAjouterActivité);
		
		btnSupprimerActivité = new JButton("Supprimer activit\u00E9 existante");
		btnSupprimerActivité.setBounds(53, 92, 305, 37);
		contentPane.add(btnSupprimerActivité);
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnRetour.setBounds(335, 227, 89, 23);
		contentPane.add(btnRetour);
		
		JButton btnModifierActivitExistante = new JButton("Modifier activit\u00E9 existante");
		btnModifierActivitExistante.setBounds(53, 151, 305, 37);
		contentPane.add(btnModifierActivitExistante);
	}
	
	public void assignListener(Ctrl ctrl) {
		this.btnAjouterActivité.setActionCommand("GestionActivites_btnAjouterActivité");
		this.btnAjouterActivité.addActionListener(ctrl);
		this.btnSupprimerActivité.setActionCommand("GestionActivites_btnSupprimerActivité");
		this.btnSupprimerActivité.addActionListener(ctrl);

	}
	
}
