package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import model.Activite;
import model.Senior;
/**
 * Classe d�finissant la vue de choix d'un s�nior pour effectuer une inscription
 * @author xavier
 *
 */
public class ChoixSeniorList extends JDialog implements MyView{

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private static JList<String> listSeniors;
	private static JList<String> listAct;
	private static JScrollPane scrollPaneA;
	private static JScrollPane scrollPaneS;
	
	/**
	 * M�thode permettant de mettre � jour le contenu de la liste des s�niors
	 * @param liste Un objet ArrayList contenant des objets Senior � int�grer dans l'ihm
	 */
	public static void setListSeniors(List<Senior> liste){
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Senior s : liste){
			model.addElement(s.getNom());
		}
		listSeniors.setModel(model);
	}
	
	public static void setListActivite(List<Activite> listeAct){
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Activite a : listeAct){
			model.addElement(a.getDesignation());
		}
		listAct.setModel(model);
	}

	/**
	 * Create the dialog.
	 * @param liste Un objet ArrayList contenant des objets Senior � int�grer dans l'ihm
	 */
	public ChoixSeniorList(List<Senior> liste,List<Activite> listeAct) {
		setTitle("S\u00E9nior - Inscrire");
		setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		scrollPaneS = new JScrollPane();
		scrollPaneS.setBounds(41, 25, 167, 84);
		
		scrollPaneA = new JScrollPane();
		scrollPaneA.setBounds(41, 145, 167, 84);
		
		listSeniors = new JList<String>();
		listSeniors.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listSeniors.setBounds(41, 25, 167, 84);
		scrollPaneS.setViewportView(listSeniors);
		contentPanel.add(scrollPaneS);
		setListSeniors(liste);
		
		listAct = new JList<String>();
		listAct.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listAct.setBounds(41, 145, 167, 84);
		scrollPaneA.setViewportView(listAct);
		contentPanel.add(scrollPaneA);
		setListActivite(listeAct);
		
		JLabel lblSenior = new JLabel("Etape 1 : choix du s\u00E9nior");
		lblSenior.setBounds(0, 0, 434, 14);
		contentPanel.add(lblSenior);
		
		JLabel lblActivite = new JLabel("Etape 2 : choix des activit\u00E9s");
		lblActivite.setBounds(0, 120, 434, 14);
		contentPanel.add(lblActivite);
	

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton btnFermer = new JButton("Fermer");
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		buttonPane.add(btnFermer);

	}

	@Override
	public void assignListener(Ctrl ctrl) {
		
	}
}
