package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;

/**
 * Classe définissant la vue d'ajout d'une activité
 * @author toto
 *
 */
public class AjoutActivite extends JFrame {

	private JPanel contentPane;
	private static JTextField txtDesignation;
	private static JTextField txtNbPersonnesMax;
	private JButton btnValider;
	private JButton btnAnnuler;
	;
	
	/**
	 * Méthode statique permettant de réinitialiser les champs
	 */
	public static void init(){
		txtDesignation.setText("");
		txtNbPersonnesMax.setText("");
	}



	/**
	 * Create the frame.
	 */
	public AjoutActivite() {
		setTitle("Activite - Ajouter");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 509, 261);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDsignationDeLactivit = new JLabel("D\u00E9signation de l'activit\u00E9 :");
		lblDsignationDeLactivit.setBounds(52, 48, 139, 14);
		contentPane.add(lblDsignationDeLactivit);
		
		txtDesignation = new JTextField();
		txtDesignation.setBounds(201, 45, 282, 20);
		contentPane.add(txtDesignation);
		txtDesignation.setColumns(10);
		
		JLabel lblNombresDePersonnes = new JLabel("Nombres de personnes maximum pour cette activit\u00E9 :");
		lblNombresDePersonnes.setBounds(52, 87, 324, 28);
		contentPane.add(lblNombresDePersonnes);
		
		txtNbPersonnesMax = new JTextField();
		txtNbPersonnesMax.setBounds(324, 91, 159, 20);
		contentPane.add(txtNbPersonnesMax);
		txtNbPersonnesMax.setColumns(10);
		
		btnValider = new JButton("Valider ");
		btnValider.setBounds(397, 188, 89, 23);
		contentPane.add(btnValider);
		
		btnAnnuler = new JButton("retour");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAnnuler.setBounds(298, 188, 89, 23);
		contentPane.add(btnAnnuler);
		
	}
	
	public static String getTxtDesignation(){
		return txtDesignation.getText();
	}
	
	public static String getTxtNbMax(){
		return txtNbPersonnesMax.getText();
	}

	public static void focusTxtDesignation() {
		AjoutActivite.txtDesignation.requestFocus();
		
	}
	
	public void assignListener(Ctrl ctrl) {
		this.btnValider.setActionCommand("AjoutActivite_valider");
		this.btnValider.addActionListener(ctrl);
		this.btnAnnuler.setActionCommand("AjoutActivite_annuler");
		this.btnAnnuler.addActionListener(ctrl);
	}



}
