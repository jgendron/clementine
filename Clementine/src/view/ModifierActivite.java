package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JList;

public class ModifierActivite extends JFrame {

	private JPanel contentPane;
	private JTextField txtDesignation;
	private JTextField txtNbPersonnesMax;
	private JButton btnRetour;


	/**
	 * Create the frame.
	 */
	public ModifierActivite() {
		setTitle("Activite - Modifier");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDsignation = new JLabel("D\u00E9signation :");
		lblDsignation.setBounds(48, 109, 81, 14);
		contentPane.add(lblDsignation);
		
		JLabel lblNewLabel = new JLabel("Nombres de personnes maximum pour cette activit\u00E9 :");
		lblNewLabel.setBounds(48, 140, 267, 14);
		contentPane.add(lblNewLabel);
		
		txtDesignation = new JTextField();
		txtDesignation.setBounds(187, 106, 237, 20);
		contentPane.add(txtDesignation);
		txtDesignation.setColumns(10);
		
		txtNbPersonnesMax = new JTextField();
		txtNbPersonnesMax.setBounds(338, 137, 86, 20);
		contentPane.add(txtNbPersonnesMax);
		txtNbPersonnesMax.setColumns(10);
		
		btnRetour = new JButton("Retour");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				GestionActivites frame = new GestionActivites();
				frame.setVisible(true);
				ModifierActivite.this.setVisible(false);
			}
		});
		btnRetour.setBounds(158, 227, 89, 23);
		contentPane.add(btnRetour);
		
		JLabel lblNewLabel_1 = new JLabel("Activit\u00E9 \u00E0 modifier :");
		lblNewLabel_1.setBounds(48, 56, 115, 14);
		contentPane.add(lblNewLabel_1);
		
		JButton btnEnregistrerModifs = new JButton("Enregistrer modifications");
		btnEnregistrerModifs.setBounds(257, 227, 167, 23);
		contentPane.add(btnEnregistrerModifs);
		
		JList list = new JList();
		list.setBounds(187, 39, 237, 43);
		contentPane.add(list);
	}
	
	
}
