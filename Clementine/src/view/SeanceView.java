package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SeanceView extends JFrame {

	private JPanel contentPane;
	private static final long serialVersionUID = 1L;
	private JButton btnEnregistrerSeance;
	
	/**
	 * Create the frame.
	 */
	public SeanceView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 556, 470);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnEnregistrerSeance = new JButton("Enregistrer seance");
		btnEnregistrerSeance.setBounds(163, 109, 192, 23);
		contentPane.add(btnEnregistrerSeance);
	}

	public void assignListener(Ctrl ctrl) {
		this.btnEnregistrerSeance.setActionCommand("SeanceView_enrseance");
		this.btnEnregistrerSeance.addActionListener(ctrl);
	}
}
