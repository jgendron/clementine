package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


import model.Activite;
import model.Seance;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class visualiserseance extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					visualiserseance frame = new visualiserseance();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public visualiserseance() {
		
		ArrayList<String> list = new ArrayList<String>();
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Activite.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
			try {
		

		List<Activite> activites = session.createQuery("from Activite").getResultList();

		for (Activite activite : activites) { 
			list.add(activite.getDesignation());
			}

		session.getTransaction().commit();
		System.out.println(list);
		
			}
			
			catch (Exception e) {e.printStackTrace();}
			finally {factory.close();}
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblVisualisationSceances = new JLabel("Visualisation sceances");
		lblVisualisationSceances.setBounds(148, 11, 213, 20);
		contentPane.add(lblVisualisationSceances);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(58, 49, 330, 49);
		contentPane.add(scrollPane);
		Vector test = new Vector();
		
		for (int i = 0; i< list.size(); i++){
				
		test.add(list.get(i));}
		
		JList liste = new JList(test);
		scrollPane.setViewportView(liste);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(58, 177, 322, 73);
		contentPane.add(textPane);
		
		JButton btnR = new JButton("Voir");
		btnR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				SessionFactory factory = new Configuration()
						.configure("hibernate.cfg.xml")
						.addAnnotatedClass(Seance.class)
						.buildSessionFactory();
				
				Session session = factory.getCurrentSession();
				session.beginTransaction();
				
				try {
					SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
			
					
				String List = "";
					List<Seance> clients = session.createQuery("from Seance").getResultList();
					for (Seance client : clients) { System.out.println(client);}
					
					clients = session.createQuery("from Seance c where c.idact ='"+(liste.getSelectedIndex()+1)+"'").getResultList();
					for (Seance client : clients) { 
						List = List +  "Il y a une sceance le " + f.format(client.getDateseance().getTime()) +"\r\n";
						textPane.setText(List);
						}
					
					session.getTransaction().commit();
				
			}
				catch(Exception f) {
					f.printStackTrace();
			
					}
						finally {
							factory.close();
						}
				
				
				
			}
		});
		btnR.setBounds(299, 109, 89, 23);
		contentPane.add(btnR);
		
		
	}
}
