package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import library.SeniorDAO;

import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;

import controller.Ctrl;
import model.Senior;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

public class SupprSenior extends JFrame {

	private static JPanel contentPane;
	private JButton btnSuppr;
	private JButton btnModifierSenior;
	private static JList<String> listSeniors;
	private static JTextField textFieldNom;
	private static JDateChooser dateChooserNaissance;
	private static JDateChooser dateChooserDeces;
	private static JScrollPane scrollPane;
	
	public static void setListSeniors(List<Senior> liste) {
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Senior s : liste) {
			model.addElement(s.getNom());
		}
		listSeniors.setModel(model);
	}
	/**
	 * Create the frame.
	 */
	public SupprSenior(List<Senior> liste) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 707, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnSuppr = new JButton("Supprimer senior");
		btnSuppr.setBounds(409, 209, 127, 41);
		contentPane.add(btnSuppr);
		
		JButton btnAnnuler = new JButton("Quitter");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAnnuler.setBounds(572, 227, 89, 23);
		contentPane.add(btnAnnuler);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 24, 195, 213);
		
		listSeniors = new JList<String>();
		listSeniors.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listSeniors.setBounds(41, 25, 167, 84);
		scrollPane.setViewportView(listSeniors);
		contentPane.add(scrollPane);
		
		btnModifierSenior = new JButton("Modifier Senior");
		btnModifierSenior.setBounds(272, 209, 127, 41);
		contentPane.add(btnModifierSenior);
		
		JLabel label_1 = new JLabel("Nom :");
		label_1.setHorizontalAlignment(SwingConstants.RIGHT);
		label_1.setBounds(326, 58, 50, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Date de naissance :");
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setBounds(231, 103, 145, 14);
		contentPane.add(label_2);
		
		textFieldNom = new JTextField();
		textFieldNom.setColumns(10);
		textFieldNom.setBounds(409, 55, 192, 20);
		contentPane.add(textFieldNom);
		
		dateChooserNaissance = new JDateChooser();
		dateChooserNaissance.setBounds(409, 97, 192, 20);
		contentPane.add(dateChooserNaissance);
		
		JLabel lblDateDeDeces = new JLabel("Date de deces :");
		lblDateDeDeces.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDateDeDeces.setBounds(231, 144, 145, 14);
		contentPane.add(lblDateDeDeces);
		
		dateChooserDeces = new JDateChooser();
		dateChooserDeces.setBounds(409, 138, 192, 20);
		contentPane.add(dateChooserDeces);
		setListSeniors(liste);
		
	}
	
	public static void init(){
		listSeniors.repaint();
		textFieldNom.setText("");
		dateChooserNaissance.setCalendar(null);
		dateChooserDeces.setCalendar(null);
	}

	public static String getSeniorSelect() throws Exception{
		List<Senior> liste=null;
		liste = SeniorDAO.getAll();
		int i = listSeniors.getSelectedIndex();
		Senior s = liste.get(i);
		String numsecu = s.getNumSecu();
		return numsecu;
	}
	
	public static String getTxtName(){
		return textFieldNom.getText();
	}
	public static GregorianCalendar getdateChooserNaissance() {
		return (GregorianCalendar) dateChooserNaissance.getCalendar();
	}
	public static GregorianCalendar getdateChooserDeces() {
		return (GregorianCalendar)dateChooserDeces.getCalendar();
	}
	
	public void assignListener(Ctrl ctrl) {
		this.btnSuppr.setActionCommand("SupprSenior_suppr");
		this.btnSuppr.addActionListener(ctrl);
		this.btnModifierSenior.setActionCommand("SupprSenior_modif");
		this.btnModifierSenior.addActionListener(ctrl);
	}
}
