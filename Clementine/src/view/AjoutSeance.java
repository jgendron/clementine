package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import controller.Ctrl;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AjoutSeance extends JFrame {

	private JPanel contentPane;
	private JButton btnValider;
	private static JDateChooser dateChooser;

	public AjoutSeance() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 482, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAjouterUneSeance = new JLabel("Ajouter une seance");
		lblAjouterUneSeance.setBounds(185, 11, 107, 14);
		contentPane.add(lblAjouterUneSeance);
		
		dateChooser = new JDateChooser();
		dateChooser.setBounds(233, 203, 154, 20);
		contentPane.add(dateChooser);
		
		JLabel lblChoisirUneDate = new JLabel("choisir une date");
		lblChoisirUneDate.setBounds(63, 203, 98, 14);
		contentPane.add(lblChoisirUneDate);
		
		btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnValider.setBounds(233, 347, 89, 23);
		contentPane.add(btnValider);
		
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.setBounds(342, 347, 89, 23);
		contentPane.add(btnQuitter);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(185, 98, 206, 52);
		contentPane.add(scrollPane);
		
		JList list = new JList();
		scrollPane.setViewportView(list);
		
		JLabel lblChoisirUneActivit = new JLabel("choisir une activit\u00E9");
		lblChoisirUneActivit.setBounds(63, 119, 98, 14);
		contentPane.add(lblChoisirUneActivit);
	}
	
	public void assignListener(Ctrl ctrl) {
	}
	
	public static JDateChooser getDateChooser() {
		return dateChooser;
	}
}


