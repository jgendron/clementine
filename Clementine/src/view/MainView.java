package view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import library.DAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Classe d�finissant la vue g�n�rale de l'application
 * @author xavier
 *
 */
public class MainView extends JFrame implements MyView{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnSenior;
	private JButton btnInscription;
	private JButton btnSupprSenior;
	private JButton btnGestionActivites;
	private JButton btnGestionSeances;
	

	/**
	 * Launch the application.
	 * @param args arguments du main
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//Cr�ation du contr�leur
					Ctrl ctrl = Ctrl.getCtrl();
					//Cr�ation de la vue g�n�rale
					MainView frame = new MainView();
					//Assignation d'un observateur sur cette vue
					frame.assignListener(ctrl);
					//Affichage de la vue
					frame.setVisible(true);
					DAO.init();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainView() {
		setTitle("Cl�mentine - Accueil");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 365);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnSenior = new JButton("Ajout d'un nouveau s�nior");
		btnSenior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSenior.setBounds(54, 56, 219, 23);
		contentPane.add(btnSenior);
		
		btnInscription = new JButton("Inscription d'un s�nior � des activit�s");
		btnInscription.setBounds(54, 104, 309, 23);
		contentPane.add(btnInscription);
		
		JButton btnFermer = new JButton("Quitter");
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnFermer.setBounds(335, 292, 89, 23);
		contentPane.add(btnFermer);

		btnGestionActivites = new JButton("Gestion Activit\u00E9s");
		btnGestionActivites.setBounds(54, 198, 309, 23);
		contentPane.add(btnGestionActivites);
		
		btnSupprSenior = new JButton("Modifier");
		btnSupprSenior.setBounds(274, 56, 89, 23);
		contentPane.add(btnSupprSenior);
		
		btnGestionSeances = new JButton("Gestion seances");
		btnGestionSeances.setBounds(54, 149, 309, 23);
		contentPane.add(btnGestionSeances);
	}

	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnSenior.setActionCommand("MainView_senior");
		this.btnSenior.addActionListener(ctrl);
		this.btnSupprSenior.setActionCommand("MainView_supprSenior");
		this.btnSupprSenior.addActionListener(ctrl);
		this.btnInscription.setActionCommand("MainView_inscription");
		this.btnInscription.addActionListener(ctrl);
		this.btnGestionSeances.setActionCommand("MainView_GestionSeances");
		this.btnGestionSeances.addActionListener(ctrl);
		this.btnGestionActivites.setActionCommand("MainView_GestionActivites");
		this.btnGestionActivites.addActionListener(ctrl);
	}
}
