package library;

import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.Activite;
import model.Senior;
import view.SupprSenior;
/**
 * Classe de persistance des objets dans une base SQL
 * @author xavier
 *
 */
public abstract class SeniorDAO {
	
	public static Session session;

	
	/**
	 * M�thode de getAll des s�niors
	 * @return un objet List contenant tous les objets Senior
	 * @throws Exception e l'exception lev�e par Hibernate
	 */	
	public static List<Senior> getAll() throws Exception{
		
		session = DAO.factory.getCurrentSession();
		
		Transaction transaction = session.beginTransaction();
		
		List<Senior> seniors;
		
		try{
			
			seniors=session.createQuery("from Senior s WHERE s.actif = 0").getResultList();
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}

		return seniors;
	}

	
	/**
	 * M�thode d'INSERT d'un nouveau s�nior
	 * @param le senior � ins�rer
	 * @return le senior
	 * @throws Exception l'exception lev�e
	 */
	public static Senior insert(Senior s) throws Exception{

		session = DAO.factory.getCurrentSession();

		Transaction transaction = session.beginTransaction();

		try{
			session.save(s);
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}
		
		return s;
	}
	
	public static String suppr(String s) throws Exception{

		session = DAO.factory.getCurrentSession();

		Transaction transaction = session.beginTransaction();

		try{
			Senior senior = session.get(Senior.class,s);
			senior.setActif(1);
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}	
		return s;
	}
	public static String modifNom(String s,String n) throws Exception{
		session = DAO.factory.getCurrentSession();
		

		Transaction transaction = session.beginTransaction();

		try{
			Senior senior = session.get(Senior.class,s);
			
			senior.setNom(n);
			
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}	
		return s;
	}
	public static String modifDateNaissance(String s,GregorianCalendar d) throws Exception{
		session = DAO.factory.getCurrentSession();
		

		Transaction transaction = session.beginTransaction();

		try{
			Senior senior = session.get(Senior.class,s);
			
			senior.setDateNaissance(d);
			
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}	
		return s;
	}
	public static String modifDateDeces(String s,GregorianCalendar d) throws Exception{
		session = DAO.factory.getCurrentSession();
		

		Transaction transaction = session.beginTransaction();
		try{
			Senior senior = session.get(Senior.class,s);
			
			senior.setDateDeces(d);
			
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}	
		return s;
	}
	
	
	
}
