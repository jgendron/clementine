package library;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.Senior;
import model.Activite;

public abstract class ActiviteDAO {

	
	public static Session session;
		
	
	/**
	 * M�thode de getAll des activit�s
	 * @return un objet List contenant tous les objets Activite
	 * @throws Exception e l'exception lev�e par Hibernate
	 */	
	public static List<Activite> getAll() throws Exception{
		
		session = DAO.factory.getCurrentSession();
		
		Transaction transaction = session.beginTransaction();
		
		List<Activite> activites;
		
		try{
			
			activites=session.createQuery("from Activite a WHERE a.actif = 0").getResultList();
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}

		return activites;
	}
	
	/**
	 * M�thode d'INSERT d'une nouvelle activit�
	 * @param l'activit� � ins�rer
	 * @return l'activit�
	 * @throws Exception l'exception lev�e
	 */
	public static Activite insert(Activite a) throws Exception{

		session = DAO.factory.getCurrentSession();

		Transaction transaction = session.beginTransaction();

		try{

			session.save(a);
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}
		
		return a;
	}
	
	public static int suppr(int s) throws Exception{

		session = DAO.factory.getCurrentSession();

		Transaction transaction = session.beginTransaction();

		try{
			Activite act = session.get(Activite.class,s);
			act.setActifA(1);;
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}	
		return s;
	}
}
