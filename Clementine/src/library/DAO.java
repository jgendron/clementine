package library;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DAO {

	public static SessionFactory factory;
	public static Session session;
	
	public static void init()
	{
		factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(model.Senior.class)
				.addAnnotatedClass(model.Activite.class)
				.buildSessionFactory();
	}
	
	public static void end()
	{
		factory.close();
	}
	
}
