package library;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.Seance;
/**
 * Classe de persistance des objets dans une base SQL
 * @author xavier
 *
 */
public abstract class SeanceDAO {
	
	public static SessionFactory factory;
	public static Session session;
	
	public static void init()
	{
		factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(model.Seance.class)
				.buildSessionFactory();
	}
	
	public static void end()
	{
		factory.close();
	}
	
	/**
	 * M�thode de getAll des seance
	 * @return un objet List contenant tous les objets Seance
	 * @throws Exception e l'exception lev�e par Hibernate
	 */	
	public static List<Seance> getAll() throws Exception{
		
		session = factory.getCurrentSession();
		
		Transaction transaction = session.beginTransaction();
		
		List<Seance> seance;
		
		try{
			
			seance=session.createQuery("from Seance").getResultList();
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}

		return seance;
	}

	
	/**
	 * M�thode d'INSERT d'une nouvelle seance
	 * @param la seance � ins�rer
	 * @return la seance
	 * @throws Exception l'exception lev�e
	 */
	public static Seance insert(Seance s) throws Exception{

		session = factory.getCurrentSession();

		Transaction transaction = session.beginTransaction();

		try{

			session.save(s);
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}
		
		return s;
	}
	

	
	
	

	
}
