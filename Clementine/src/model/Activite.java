package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="activite")

public class Activite {
	
	public Activite () {
		super();
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator ="activite_generator")
	@SequenceGenerator(name="activite_generator",sequenceName="activite_generator", allocationSize = 1)
	@Column(name="identifiant")
	private Integer id;
	
	@Column(name="designation")
	private String designation;
	
	@Column(name="nbmax")
	private int nbmax;
	
	@Column(name="actif")
	private int actif;

	public Activite(String designation, int nbmax, int actif) {
		this.designation = designation;
		this.nbmax = nbmax;
		this.actif = actif;
	}

	public Integer getId() {
		return id;
	}

	public String getDesignation() {
		return designation;
	}
	
	public int getActifA() {
		return actif;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public int getNbmax() {
		return nbmax;
	}

	public void setNbmax(int nbmax) {
		this.nbmax = nbmax;
	}
	
	public void setActifA(int actif) {
		this.actif = actif;
	}
	
	
	
}
