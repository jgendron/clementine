package model;

import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe d'objet m�tier SEANCE
 * @author toto montiel
 *
 */

@Entity
@Table(name="seance")

public class Seance {
	public Seance() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator ="seance_generator")
	@SequenceGenerator(name="seance_generator",sequenceName="seance_generator", allocationSize = 1)
	
	@Column(name="code")
	private int code;
	

	@Column(name="dateseance")
	private GregorianCalendar dateseance;

	
	@Column(name="idact")
	private int idact;

	public Seance(GregorianCalendar dateseance, int idact) {
		super();
		this.code = code;
		this.dateseance = dateseance;
		this.idact = idact;
	}


	public GregorianCalendar getDateseance() {
		return dateseance;
	}

	public int getIdact() {
		return idact;
	}


}
