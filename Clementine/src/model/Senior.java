package model;

import java.time.LocalDate;
import java.time.Period;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe d'objet m�tier SENIOR
 * @author xavier & maelle
 *
 */

@Entity
@Table(name="senior")
public class Senior {
	/**
	 * Le num�ro de s�curit� sociale du s�nior
	 */
	@Id
	@Column(name="numsecu")
	private String numSecu;
	/**
	 * Le nom du s�nior
	 */
	@Column(name="nom")
	private String nom;
	/**
	 * La date de naissance du s�nior
	 */
	
	@Column(name="dateNaissance")
	private GregorianCalendar dateNaissance;
	/**
	 * La date de d�c�s du s�nior
	 */
	@Column(name="dateDeces")
	private GregorianCalendar dateDeces;
	
	@Column(name="actif")
	private int actif;
	
	
	/**
	 * CONSTRUCTEUR de la classe Senior
	 * @param numSecu num�ro de s�curit� sociale du nouveau s�nior
	 * @param nom nom du nouveau s�nior
	 * @param date de naissaince du nouveau s�nior
	 */
	public Senior(String numSecu, String nom, GregorianCalendar dateNaiss, int actif) {
		super();
		this.numSecu = numSecu;
		this.nom = nom;
		this.dateNaissance=dateNaiss;
		this.dateDeces=null;
		this.actif=actif;
	}
	
	/**
	 * CONSTRUCTEUR de la classe Senior
	 * @param numSecu num�ro de s�curit� sociale du nouveau s�nior
	 * @param nom nom du nouveau s�nior
	 * @param date de naissaince du nouveau s�nior
	 */
	public Senior() {
		super();
	}

	/**
	 * Accesseur en lecture sur la date de naissance du s�nior
	 * @return la date de naissance du s�nior
	 */
	public GregorianCalendar getDateNaissance() {
		return dateNaissance;
	}

	/**
	 * Accesseur en lecture sur la date de d�c�s du s�nior
	 * @return la date de d�c�s du s�nior
	 */
	public GregorianCalendar getDateDeces() {
		return dateDeces;
	}

	/**
	 * Accesseur en lecture sur le num�ro de s�curit� sociale du s�nior
	 * @return le num�ro de s�curit� sociale du s�nior
	 */
	public String getNumSecu() {
		return numSecu;
	}

	/**
	 * Accesseur en lecture sur le nom du s�nior
	 * @return le nom du s�nior
	 */
	public String getNom() {
		return nom;
	}
	
	public int getActif() {
		return actif;
	}
	
	
	/**
	 * Permet de calculer l'age du s�nior � une date 
	 * @param uneDate
	 * @return un entier repr�sentant l'age du senior � la date pass�e en param�tre
	 */
	
	
	public int donneAge(LocalDate uneDate) {
		
		LocalDate birthDate = LocalDate.of(this.dateNaissance.get(GregorianCalendar.YEAR), this.dateNaissance.get(GregorianCalendar.MONTH), this.dateNaissance.get(GregorianCalendar.DAY_OF_MONTH));
		return Period.between(birthDate, uneDate).getYears();
		
	}

	public void setNumSecu(String numSecu) {
		this.numSecu = numSecu;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setDateNaissance(GregorianCalendar dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public void setDateDeces(GregorianCalendar dateDeces) {
		this.dateDeces = dateDeces;
	}
	
	public void setActif(int actif) {
		this.actif = actif;
	}
}
