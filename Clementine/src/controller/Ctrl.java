package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JOptionPane;

import com.toedter.calendar.JDateChooser;

import library.SeanceDAO;
import library.ActiviteDAO;
import library.SeniorDAO;
import model.Activite;
import model.Seance;
import model.Senior;
import view.GestionActivites;
import view.AjoutActivite;
import view.AjoutSeance;
import view.AjoutSenior;
import view.ChoixSeniorList;
import view.SupprSenior;
import view.SupprimerActivite;
import view.SeanceView;

/**
 * Classe CONTROLEUR
 * @author xavier
 *
 */
public class Ctrl implements ActionListener{

	/**
	 * CONSTRUCTEUR priv� de la classe Ctrl
	 */
	private Ctrl(){
	}
	/**
	 * Instance unique pr�-initialis�e
	 */
	private static Ctrl CTRL = new Ctrl();
	/**
	 * Point d'acc�s pour l'instance unique du singleton
	 * @return le singleton Ctrl
	 */
	public static Ctrl getCtrl(){
		return CTRL;
	}

	/**
	 * M�thode d�clanch�e lors de clics sur les boutons de l'application
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		//R�cup�ration de l'action
		String action = evt.getActionCommand();
		//D�coupage et analyse de celle-ci
		String details[] = action.split("_");
		//who : QUI ? Quelle vue a effectu� l'action ?
		String who = details[0];
		//what : QUOI ? Qu'est-ce-que cette vue souhaite faire ?
		String what = details[1];
		//switch-case de traitement des diff�rents cas
		switch(who){
		case "MainView":
			switch(what){
			case "senior":
				//Cr�ation de la vue d'ajout d'un s�nior
				AjoutSenior frame = new AjoutSenior();
				//Assignation d'un observateur sur cette vue
				frame.assignListener(this);
				//Affichage de la vue
				frame.setVisible(true);
				break;
			case "GestionActivites":
				//Cr�ation de la vue d'ajout d'une activite
				GestionActivites frame2 = new GestionActivites();
				//Assignation d'un observateur sur cette vue
				frame2.assignListener(this);
				//Affichage de la vue
				frame2.setVisible(true);
				break;	
			case "inscription":
				//Cr�ation de la vue de s�lection du s�nior pour lequel on souhaite effectuer des inscriptions
				List<Senior> liste=null;
				List<Activite> listeAct=null;
				try {
					liste = SeniorDAO.getAll();
					listeAct = ActiviteDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				ChoixSeniorList frame1 = new ChoixSeniorList(liste,listeAct);
				//Assignation d'un observateur sur cette vue
				frame1.assignListener(this);
				//Affichage de la vue
				frame1.setVisible(true);
				break;
			case "supprSenior":
				//Cr�ation de la vue d'ajout d'un s�nior
				List<Senior> liste2=null;
				try {
					liste2 = SeniorDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				SupprSenior frame6 = new SupprSenior(liste2);
				//Assignation d'un observateur sur cette vue
				frame6.assignListener(this);
				//Affichage de la vue
				frame6.setVisible(true);
				break;
			case "GestionSeances":
				//Cr�ation de la vue d'ajout d'un s�nior
				SeanceView frame3 = new SeanceView();
				//Assignation d'un observateur sur cette vue
				frame3.assignListener(this);
				//Affichage de la vue
				frame3.setVisible(true);
				break;
			}
			break;
			
		case "AjoutSenior":
			switch(what){
			case "valider":
				//R�cup�ration des informations saisies par l'utilisateur
				String nom = AjoutSenior.getTxtName();
				if(nom.equals("")){
					JOptionPane.showMessageDialog(null,"Le nom du s�nior � �t� omis. Veillez � le saisir correctement.","Erreur Saisie",JOptionPane.WARNING_MESSAGE);
					AjoutSenior.focusTxtName();
				}
				else{
					String nomS = AjoutSenior.getTxtName();
					String numS = AjoutSenior.getTxtNumSecu();
					GregorianCalendar dateN = AjoutSenior.getdateChooserNaissance();
					//Cr�ation du nouvel objet Senior
					Senior senior = new Senior(numS,nomS,dateN,0);
					//INSERT dans la BD
					try {
						SeniorDAO.insert(senior);
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"Le s�nior a bien �t� ajout�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
						//R�initialisation des champs
						AjoutSenior.init();
					} catch (Exception e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
					}
				break;
				}
			break;
			case "annuler":
				//R�initialisation des champs
				AjoutSenior.init();
				break;
			}
			break;
			
		case "SupprSenior" :
			switch(what){
			case "suppr" :
				String senior;
				try {
					senior = SupprSenior.getSeniorSelect();
					SeniorDAO.suppr(senior);
					//Message de confirmation pour l'utilisateur
					JOptionPane.showMessageDialog(null,"Le s�nior a bien �t� supprim�","Confirmation Suppression",JOptionPane.INFORMATION_MESSAGE);
					SupprSenior.setListSeniors(SeniorDAO.getAll());
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				break;
			case "modif" :
				if(!SupprSenior.getTxtName().equals("")) {
					String s;
					try {
						s = SupprSenior.getSeniorSelect();
						SeniorDAO.modifNom(s,SupprSenior.getTxtName());
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"Le nom du s�nior a bien �t� modifi�","Confirmation modification",JOptionPane.INFORMATION_MESSAGE);
						SupprSenior.setListSeniors(SeniorDAO.getAll());
					} catch (Exception e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
					}
					SupprSenior.init();
				}
				if(SupprSenior.getdateChooserNaissance() != null) {
					String s;
					try {
						s = SupprSenior.getSeniorSelect();
						SeniorDAO.modifDateNaissance(s,SupprSenior.getdateChooserNaissance());
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"La date de naissance du s�nior a bien �t� modifi�","Confirmation modification",JOptionPane.INFORMATION_MESSAGE);
						SupprSenior.setListSeniors(SeniorDAO.getAll());
					} catch (Exception e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
					}
					SupprSenior.init();
				}
				if(SupprSenior.getdateChooserDeces() != null) {
					String s;
					try {
						s = SupprSenior.getSeniorSelect();
						SeniorDAO.modifDateDeces(s, SupprSenior.getdateChooserDeces());
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"La date de naissance du s�nior a bien �t� modifi�","Confirmation modification",JOptionPane.INFORMATION_MESSAGE);
						SupprSenior.setListSeniors(SeniorDAO.getAll());
					} catch (Exception e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
					}
					SupprSenior.init();
				}
				break;
			}
			break;
			
		case "InscriptionSenior":
			break;
			
		case "SeanceView":
			switch(what){
			case "enrseance":
				//Cr�ation de la vue d'ajout d'un s�nior
				AjoutSeance frameajoutseance = new AjoutSeance();
				//Assignation d'un observateur sur cette vue
				frameajoutseance.assignListener(this);
				//Affichage de la vue
				frameajoutseance.setVisible(true);
				break;
			}
			break;
			
		case "AjoutSeance":
			switch(what) {
			case "valider":
				SeanceView frameseance = new SeanceView();
				//Assignation d'un observateur sur cette vue
				frameseance.assignListener(this);
				//Affichage de la vue
				frameseance.setVisible(true);
		
				
				//R�cup�ration des informations saisies par l'utilisateur			
				GregorianCalendar date =(GregorianCalendar)AjoutSeance.getDateChooser().getCalendar();
					
					//Cr�ation du nouvel objet seance
					Seance seance = new Seance(date,2);
					//INSERT dans la BD
					try {
						SeanceDAO.insert(seance);
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"La seance a bien �t� ajout�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
						//R�initialisation des champs
						AjoutSenior.init();
					} catch (Exception e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
					}
					break;
				}
			break;
			
		case "GestionActivites":
			switch(what) {
			case "btnAjouterActivit�":
				//Cr�ation de la vue d'ajout d'un s�nior
				AjoutActivite frame4 = new AjoutActivite();
				//Assignation d'un observateur sur cette vue
				frame4.assignListener(this);
				//Affichage de la vue
				frame4.setVisible(true);
				break;
			case "btnSupprimerActivit�":
				//Cr�ation de la vue d'ajout d'une Activite
				List<Activite> liste3=null;
				try {
					liste3 = ActiviteDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				SupprimerActivite frame9 = new SupprimerActivite(liste3);
				//Assignation d'un observateur sur cette vue
				frame9.assignListener(this);
				//Affichage de la vue
				frame9.setVisible(true);
				break;
			}
			break;
		case "AjoutActivite":
			switch(what){
			case "valider":
				//R�cup�ration des informations saisies par l'utilisateur
				JOptionPane.showMessageDialog(null,"L'activit� a bien �t� ajout�e","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
				String designation = AjoutActivite.getTxtDesignation();
				if(designation.equals("")){
					JOptionPane.showMessageDialog(null,"Vous avez oubli� de saisir une d�signation.","Erreur Saisie",JOptionPane.WARNING_MESSAGE);
					AjoutActivite.focusTxtDesignation();
				}
				else{
					String designationA = AjoutActivite.getTxtDesignation();
					String nbmax = AjoutActivite.getTxtNbMax();
					int nbmaximum = Integer.parseInt(nbmax);
				
					//Cr�ation du nouvel objet Activit�
					Activite activite = new Activite(designationA,nbmaximum,0);
					//INSERT dans la BD
					try {
						ActiviteDAO.insert(activite);
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"L'activit� a bien �t� ajout�e","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
						//R�initialisation des champs
						AjoutActivite.init();
					} catch (Exception e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);						}
				}
				break;
			}
			break;
		case"SupprimerActivite":
			switch(what){
			case "suppr":
				int act;
				try {
					act = SupprimerActivite.getActSelect();
					ActiviteDAO.suppr(act);
					//Message de confirmation pour l'utilisateur
					JOptionPane.showMessageDialog(null,"Le s�nior a bien �t� supprim�","Confirmation Suppression",JOptionPane.INFORMATION_MESSAGE);
					SupprimerActivite.setListActivite(ActiviteDAO.getAll());
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				break;
			}
			break;
		}
	}
}